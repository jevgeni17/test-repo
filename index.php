<?php require_once "header.php"; ?>
<?php
if(isset($_POST['submit'])){
  if(!empty($_POST['check'])){
      foreach($_POST['check'] as $id){
          $products->removeProduct($id);
      }
  } else {
      echo '<div class="alert alert-danger" role="alert">0 checkboxes selected.</div>';
  }
}

?>
<section class="jumbotron ">
  <div class="container text-left ">
    <h1>Product List</h1>
  </div>
  <div class="container text-right">
    <a href="add.php" class="btn btn-primary my-2">Add</a>
    <input type="submit" form="checkboxes" name="submit" id="submit" class="btn btn-danger" value="Mass Delete">
  </div>
</section>
<form action="" method="post" id="checkboxes" name="checkboxes">
    <?php
        $result = $products->getAllProducts();
        require_once "products.php";
    ?> 
</form>
   
