<?php
require_once ("classes/Book.php");
require_once ("classes/Database.php");
require_once ("classes/DVD.php");
require_once ("classes/Furniture.php");
require_once ("classes/Product.php");
require_once ("footer.php");

$db_handle = new DBController();
$products = new Product();
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title></title>
  </head>
  <body>

  <main role="main">