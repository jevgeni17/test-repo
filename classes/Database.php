<?php
class DBController
{
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "products";
    private $conn;

    function __construct() {
        $this->conn = $this->connectDB();
    }

    protected function connectDB() {
        $conn = mysqli_connect($this->host,$this->username,$this->password,$this->database);
        return $conn;
    }

    function runBaseQuery($query) {
        $result = $this->conn->query($query);   
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $resultset[] = $row;
            }
        }
        return $resultset;
    }

    
}