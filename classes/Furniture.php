<?php
require_once "Product.php";

class Furniture extends Product
{
    protected $height;
    protected $width;
    protected $length;

    function __construct($sku, $name, $price, $value, $type, $weight, $width, $length)
    {
        parent::__construct($sku, $name, $price, $value, $type);
        $this->weight = $weight;
        $this->width = $width;
        $this->length = $width;
    }

    public function getHeight()
    {
        return $this->height;
    }
    public function getWidth()
    {
        return $this->width;
    }
    public function getLength()
    {
        return $this->length;
    }
    public function setHeight($height)
    {
        $this->height = $height;
    }
    public function setWidth($width)
    {
        $this->width = $width;
    }
    public function setLength($length)
    {
        $this->length = $length;
    }
    
}

?>