<?php
require_once "Product.php";

class Book extends Product
{
    protected $weight;

    function __construct($sku, $name, $price, $value, $type, $weight)
    {
        parent::__construct($sku, $name, $price, $value, $type);
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight()
    {
        $this->weight = $weight;
    }
}