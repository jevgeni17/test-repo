<?php

interface IProduct {
    public function getAllProducts();
    public function addProduct($product);
    public function removeProduct($id);
}