<?php 
require_once "Database.php";
require_once "Product_Interface.php";

class Product extends DBController implements IProduct
{
    private $sku;
    private $name;
    private $price;
    private $value;
    private $type;

    private $db_handle;

    function __construct($sku = null, $name = null, $price = null, $value = null, $type = null) {
        $this->db_handle = new DBController();
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->value = $value;
        $this->type = $type;
    }

    function getAllProducts() {
        $sql = "SELECT p.product_id,
                       p.sku,
                       p.name,
                       p.price,
                       a.attribute_name,
                       t.value,
                       a.attribute_value
                       from products as p
                       left join product_attribute t
                          on p.product_id = t.product_id
                       left join attribute a
                          on t.attribute_id = a.attribute_id
                       group by p.product_id, p.name, p.price";
        $result = $this->db_handle->runBaseQuery($sql);
        return $result;
    }

    function addProduct($product) {
        $sql = "INSERT INTO products (product_id, sku, name, price) VALUES (null, '$this->sku', '$this->name', '$this->price')";
        $sql2 = "SELECT p.product_id from products as p ORDER BY product_id DESC LIMIT 1";
        $result = $this->db_handle->runBaseQuery($sql);
        $result2 = $this->connectDB()->query($sql2);
        while($row = $result2->fetch_assoc()) {
            $sql3 = "INSERT INTO product_attribute (product_id, attribute_id, value) VALUES (". $row["product_id"]. ", '$this->type', '$this->value');";
            $result3 = $this->db_handle->runBaseQuery($sql3);
        }
        return $result3;
    }

    function removeProduct($id){
        $sql = "DELETE FROM product_attribute WHERE product_id='$id';";
        $sql2 = "DELETE FROM products WHERE product_id='$id';";
        $result= $this->connectDB()->query($sql);
        $result2= $this->connectDB()->query($sql2);
        return $result2;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    public function setType($type)
    {
        $this->type = $type;
    }
    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getType()
    {
        return $this->type;
    }
    public function getValue()
    {
        return $this->value;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getSku()
    {
        return $this->sku;
    }
}