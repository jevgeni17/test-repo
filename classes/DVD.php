<?php
require_once "Product.php";

class DVD extends Product
{   
    protected $size;

    function __construct($sku, $name, $price, $value, $type, $size)
    {
        parent::__construct($sku, $name, $price, $value, $type);
        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }
    
    public function setSize($size)
    {
        $this->size = $size;
    }

}