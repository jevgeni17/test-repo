<?php require_once ("header.php"); ?>
<?php       
if (isset($_POST['product_form']))
{
    $sku = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $type = $_POST['type'];
    $value = NULL;
    switch($type){
        case 'dimensions':
            $height = $_POST['height'];
            $width = $_POST['width'];
            $length = $_POST['length'];
            $value = $height . "x" . $width . "x" . $length;
            $type = 2;

            $product = new Furniture($sku, $name, $price, $value, $type, $height, $width, $length);
        break;
        case 'weight':
            $value = $_POST['weight'];
            $type = 1;

            $product = new Book($sku, $name, $price, $value, $type, $value);
        break;
        case 'size':
            $value = $_POST['size'];
            $type = 3;
            
            $product = new DVD($sku, $name, $price, $value, $type, $value);
        break;
    }
    if($value != NULL) {
        $product->addProduct($product);
        header("Location:index.php");
    } else {
        echo 'error';
    }
}

?>

<section class="jumbotron ">
  <div class="container text-left ">
    <h1>Add Product</h1>
  </div>
  <div class="container text-right">
  <input type="submit" value="Save" form="product_form" name="product_form" class="btn btn-primary my-2">
    <a href="index.php" class="btn btn-secondary my-2">Cancel</a>
  </div>
</section>

<div class="container">
        <div class="row justify-content-center mt-3">
           <div class="col-6">
              <form id="product_form" action="" method="POST" onsubmit="return validate(this)">
                 <fieldset>
                    <label>SKU</label>
                    <input class="form-control" type="text" name="sku" id="sku" tabindex="1" required="" autofocus="">
                 </fieldset>
                 <br>
                 <fieldset>
                    <label>Name</label>
                    <input class="form-control" placeholder="" type="text" name="name" id="name" tabindex="1" required="" autofocus="">
                 </fieldset>
                 <br>
                 <fieldset>
                    <label>Price</label>
                    <input class="form-control" placeholder="" type="text" name="price" id="price" tabindex="1" required="" autofocus="">
                 </fieldset>
                 <br>
                 <fieldset>
                 <label>Type switcher</label>
                 <select class="custom-select mr-sm-2" id="select-option" name="type">
                    <option label=" "></option>
                    <option value="size">DVD-disc</option>
                    <option value="weight">Book</option>
                    <option value="dimensions">Furniture</option>
                </select>
                 </fieldset>
                 <br>
                 <fieldset id="change-input">

                 </fieldset>

              </form>
           </div>
        </div>
     </div>