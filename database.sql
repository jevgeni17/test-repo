CREATE DATABASE IF NOT EXISTS Products CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `sku` VARCHAR(30) NOT NULL UNIQUE,
  `name` VARCHAR(30) NOT NULL,
  `price` DECIMAL NOT NULL
); 

INSERT INTO `products` (`product_id`, `sku`, `name`, `price`) 
VALUES (NULL, 'JVC200', 'white fang', '10');

CREATE TABLE IF NOT EXISTS `attribute` (
  `attribute_id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `attribute_name` VARCHAR(30) NOT NULL,
  `attribute_value` VARCHAR(30) NOT NULL
);

INSERT INTO `attribute` (`attribute_id`, `attribute_name`, `attribute_value`) 
VALUES (NULL, 'weight', 'kg'), (NULL, 'dimensions', 'HxWxL'), (NULL, 'size', 'MB');

CREATE TABLE IF NOT EXISTS `product_attribute` (
    `product_id` INT(10) NOT NULL,
    `attribute_id` INT(10) NOT NULL,
    `value` VARCHAR(40) NOT NULL,
    CONSTRAINT `FK_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
    CONSTRAINT `FK_attribute_id` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`attribute_id`)
);

INSERT INTO `product_attribute` (`product_id`, `attribute_id`, `value`) 
VALUES (1, 1, '0.5');

 


