$(document).ready(function(){
    $("#select-option").on("change", function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            switch(optionValue){
                case 'size':
                    $("#change-input").html(`
                        <label>Size(MB)</label>
                            <input class="form-control" placeholder="" type="text" name="size" id="size" tabindex="1"  autofocus=""><br>
                            <div class="alert alert-primary" role="alert">Please provide DVD-disc size.</div>
                    `);
                    break;
                case 'weight':
                    $("#change-input").html(`
                        <label>Weight(KG)</label>
                            <input class="form-control" placeholder="" type="text" name="weight" id="weight" tabindex="1" required="" autofocus=""><br>
                            <div class="alert alert-primary" role="alert">Please provide book weight.</div>
                    `);
                    break;
                case 'dimensions':
                    $("#change-input").html(`
                        <label >Height(CM)</label>
                            <input class="form-control" placeholder="" type="text" name="height" id="height" tabindex="1" required="" autofocus=""><br>
                        <label>Width(CM)</label>
                            <input class="form-control" placeholder="" type="text" name="width" id="width" tabindex="1" required="" autofocus=""><br>
                        <label>Length(CM)</label>
                            <input class="form-control" placeholder="" type="text" name="length" id="length" tabindex="1" required="" autofocus=""><br>
                            <div class="alert alert-primary" role="alert">Please provide dimensions in HxWxL format.</div>
                    `);
                    break;
            }
        });
})
});