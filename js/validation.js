/**
 * validate all form conditions
 */
function validate() { 
	var sku =  $('#sku').val();
    var name = $('#name').val();
    var price = parseFloat($('#price').val());
    var type_switcher = $("#select-option option:selected").val();
    
    var input_size = $("#size").val();
    var input_weight = $("#weight").val();

    var input_height = $("#height").val();
    var input_width = $("#width").val();
    var input_length = $("#length").val();
	
	//sku validation
	if(sku.length == 0){
		document.getElementById("sku").style.borderColor = "red";
		return false;
	}else{
		document.getElementById("sku").style.borderColor = "green";
    }

    //name validation
    if(name.length == 0){
        document.getElementById("name").style.borderColor = "red";
		return false;
    }else{
        document.getElementById("name").style.borderColor = "green";
    }

    //price validation
    if(price.length == 0 || isNaN(price)){
        document.getElementById("price").style.borderColor = "red";
		return false;
    }else{
        document.getElementById("price").style.borderColor = "green";
    }
    
    //switcher validation
    if(type_switcher != 'size' && type_switcher != 'weight' && type_switcher != 'dimensions'){
        document.getElementById("select-option").style.borderColor = "red";
		return false;
    }else{
		document.getElementById("select-option").style.borderColor = "green";
    }

    

    switch (type_switcher) {
        case 'size':
          if(isNaN(input_size) || input_size.length == 0){
            document.getElementById("size").style.borderColor = "red";
            return false;
          }else{
            document.getElementById("size").style.borderColor = "green";
          }
          break;
        case 'weight':
            if(isNaN(input_weight) || input_weight.length == 0){
                document.getElementById("weight").style.borderColor = "red";
                return false;
            }else{
                document.getElementById("weight").style.borderColor = "green";
            }
          break;
        case 'dimensions':
            if(isNaN(input_height) || input_height.length == 0){
                document.getElementById("height").style.borderColor = "red";
                return false;
            }else{
                document.getElementById("height").style.borderColor = "green";
            }

            if(isNaN(input_width) || input_width.length == 0){
                document.getElementById("width").style.borderColor = "red";
                return false;
            }else{
                document.getElementById("width").style.borderColor = "green";
            }

            if(isNaN(input_length) || input_length.length == 0){
                document.getElementById("length").style.borderColor = "red";
                return false;
            }else{
                document.getElementById("length").style.borderColor = "green";
            }
          break;
        default:
          alert( "" );
      }

    return true;
}