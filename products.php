
<div class="album">
    <div class="container">
        <div class="row ">
        <?php
            if(!empty($result)){
                foreach($result as $res){
                    ?>
                    <div class="col-md-4 mt-5 ">
                        <div class="card" style="width: 18rem;">
                            <div class="card-header">
                                <input class="checkBoxes" type="checkbox"  name="check[]" value="<?php echo $res['product_id'] ?>"> 
                            </div>
                            <div class="card-body">
                                <h5 class="card-title text-center"><?php echo $res['sku'] ?></h5>
                                <h6 class="card-text text-center"><?php echo $res['name'] ?></h6>
                                <p class="card-text text-center"><?php echo $res['price'] ?>$</p>
                                <p class="card-text text-center"><?php echo $res['attribute_name'] ?>: <?php echo $res['value'] ?> <?php echo $res['attribute_value'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php } 
            }?>
        </div>
    </div>
</div>





  